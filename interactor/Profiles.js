import OlaNordmann from "./MockContent/OlaNordmann.js";
import NilsJakobsen from "./MockContent/NilsJakobsen.js";
import JakobOlsen from "./MockContent/JakobOlsen.js";
import FridaJensen from "./MockContent/FridaJensen.js";
import SynneThune from "./MockContent/SynneThune.js";
import ElisabethLarsen from "./MockContent/ElisabethLarsen.js";
import {requiredParameter} from "../Utils/Tools.js";

const TAG = "Profiles";

//Type: class Profile
const profiles = [
    new OlaNordmann(),
    new NilsJakobsen(),
    new JakobOlsen(),
    new FridaJensen(),
    new SynneThune(),
    new ElisabethLarsen()
]

export default
class Profiles {

    /**
     *
     * @returns Array containing the different implementations of Profile.js
     */
    getProfiles() {
        return profiles;
    }

    getBasedOnGender(gender = requiredParameter(TAG, "gender", "getBasedOnGender")) {
        let out = [];

        profiles.forEach( profile => {
            if (profile.gender === gender) out.push(profile);
        });

        return out;
    }
}