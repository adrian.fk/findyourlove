export default
class ProfilesAdapter {

    static filterBasedOnAge(profiles, age) {
        let out = []
        if (age >= 25) {
            alert("Alder over 25")
            for (let i = 0; i < profiles.length; i++) {
                if (profiles[i].age >= 25)
                    out.push(profiles[i]);
            }
        }
        else {
            for (let i = 0; i < profiles.length; i++) {
                if (profiles[i].age < 25)
                    out.push(profiles[i]);
            }
        }

        return out;
    }

    static filterBasedOnGender(profiles = [], gender) {
        //return profiles.filter(profile => {return profile === gender} )

        let out = [];

        for (let i = 0; i < profiles.length; i++) {
            if (profiles[i].gender === gender) out.push(profiles[i])
        }

        return out;
    }
}