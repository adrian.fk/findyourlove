export const GenderTypes = {
    MALE: "MALE",
    FEMALE: "FEMALE"
}

/**
 *  The purpose of having the MockContent hardcoded like this is to "simulate" a database layer that our Interactor could
 *  and should interact with and source it's information from through it's proprietary gateway.
 */

const description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/> Fusce lacus risus, sodales et luctus id, vestibulum quis leo. Praesent mi nisl, pharetra eu nunc in, rhoncus accumsan justo. Donec sapien felis, mollis vel quam id, viverra sodales quam. ";

export default
class Profile {

    constructor(imgUrl, name, gender, age) {
        this._imgUrl = imgUrl;
        this._name = name;
        this._gender = gender;
        this._age = age;
        this._description = description;
    }


    get imgUrl() {
        return this._imgUrl;
    }

    set imgUrl(value) {
        this._imgUrl = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get gender() {
        return this._gender;
    }

    set gender(value) {
        this._gender = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get description() {
        return this._description;
    }

    set description(value) {
        this._description = value;
    }
}