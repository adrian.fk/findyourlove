import Profile, {GenderTypes} from "../model/Profile.js";

const profileName = "Frida Jensen";
const imgUrl = "images/woman1.jpg";
const gender = GenderTypes.FEMALE;
const age = 27;
const description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br/> Fusce lacus risus, sodales et luctus id, vestibulum quis leo. Praesent mi nisl, pharetra eu nunc in, rhoncus accumsan justo. Donec sapien felis, mollis vel quam id, viverra sodales quam. Sed pulvinar cursus hendrerit. Quisque sit amet mattis ligula. Fusce enim urna, vulputate et augue sed, viverra pharetra dui. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam in feugiat eros. Fusce ante nibh, suscipit et vestibulum at, porttitor a neque. In viverra, lectus ut aliquam commodo, mauris sem tristique nulla, sed faucibus mauris nunc at augue. Mauris volutpat dignissim lorem id auctor. Cras et porta nisi.\n";

export default
class FridaJensen extends Profile {

    constructor() {
        super(imgUrl, profileName, gender, age);
    }
}