import {requiredParameter} from "./Utils/Tools.js";

const TAG = "Header";

export const MenuItems = {
    HOME: "Hjem",
    ALL_PROFILES: "Alle profiler",
    MATCH: "Profil forslag",
    SECRET_WONDERER: "Hemmelig beundrer"
}

export const MenuItemIDs = {
    HOME: "home",
    ALL_PROFILES: "all-profiles",
    MATCH: "match",
    SECRET_WONDERER: "secret-admirer"
}

function navTo(destination) {
    switch (destination) {
        case MenuItems.HOME:
            window.location.assign("index.html");
            break;

        case MenuItems.ALL_PROFILES:
            window.location.assign("alle-profiler.html")
            break;

        case MenuItems.SECRET_WONDERER:
            window.location.assign("secret-admirer.html")
            break;


        case MenuItems.MATCH:
            window.location.href = "profil-forslag.html"
    }
}


export default
class Header {

    static getInstance() {
        if (!Header.instance) {
            Header.instance = new Header();
        }
        return Header.instance;
    }

    constructor() {
        return Header.getInstance();
    }

    static targetMenuEntry(menuItemEntry) {
        const menuOption = document.querySelectorAll('.menu_option__entry');

        menuOption.forEach(option => {
            if (option.innerHTML.includes(menuItemEntry)) {
                option.classList.add('menu_option--active');
            }
            else {
                option.classList.remove('menu_option--active');
            }
        });
    }

    static #html() {
        return `
            <section id='header' class='header'>
                <section class='header__logo'>
                    <img src='images/logo.png' alt='<3' />
                    <h2>FindYourLove</h2>
                </section>
                <nav class='header__menu'>
                    <section id="menu__${MenuItemIDs.HOME}" class='menu_option'>
                        <h4 class='menu_option__entry'>
                            ${MenuItems.HOME}
                        </h4>    
                    </section>
                    <section id="menu__${MenuItemIDs.ALL_PROFILES}" class='menu_option'>
                        <h4 class='menu_option__entry'>
                                ${MenuItems.ALL_PROFILES}
                        </h4>
                    </section>
                    <section id="menu__${MenuItemIDs.MATCH}" class='menu_option'>
                        <h4 class='menu_option__entry'>
                            ${MenuItems.MATCH}
                        </h4>
                    </section>
                    <section id="menu__${MenuItemIDs.SECRET_WONDERER}" class='menu_option'>
                        <h4 class='menu_option__entry'>
                                ${MenuItems.SECRET_WONDERER}
                            </h4>
                    </section>
                </nav>
            </section>
        `
    }

    static render(menuItemEntry = requiredParameter(TAG, "menuItemEntry", "render()")) {

        document.body.insertAdjacentHTML('afterbegin', Header.#html());
        Header.targetMenuEntry(menuItemEntry);
        this.registerListeners();
    }

    static registerListeners() {
        document.getElementById(`menu__${MenuItemIDs.HOME}`).onclick = () => navTo(MenuItems.HOME);
        document.getElementById(`menu__${MenuItemIDs.ALL_PROFILES}`).onclick = () => navTo(MenuItems.ALL_PROFILES);
        document.getElementById(`menu__${MenuItemIDs.MATCH}`).onclick = () => navTo(MenuItems.MATCH);
        document.getElementById(`menu__${MenuItemIDs.SECRET_WONDERER}`).onclick = () => navTo(MenuItems.SECRET_WONDERER);
    }
}