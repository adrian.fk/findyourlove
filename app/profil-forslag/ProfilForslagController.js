import ProfilForslagView from "./view/ProfilForslagView.js";
import Profiles from "../../interactor/Profiles.js";
import ProfilesAdapter from "../../interactor/Adapter/ProfilesAdapter.js";

export default
class ProfilForslagController {

    constructor() {
        this.view = new ProfilForslagView();
    }

    init() {
        this.view.init();
        this.registerListeners();
    }


    handleSubmitBtnClick() {
        const profiles = new Profiles().getProfiles();
        const v = new ProfilForslagView();
        const age = v.getAgeInput();
        const gender = v.getGenderInput();

        //v.toggleStageFadeAnim();

        let o = ProfilesAdapter.filterBasedOnGender(profiles, gender);
        o = ProfilesAdapter.filterBasedOnAge(o, age);

        let rdmProfileIndex = Math.floor(Math.random() * o.length);
        let selectedProfile = o[rdmProfileIndex];

        v.displayProfile(selectedProfile.name, selectedProfile.imgUrl, selectedProfile.age, selectedProfile.description);

        /*
        setTimeout(() => {
            v.displayProfile(selectedProfile.name, selectedProfile.imgUrl, selectedProfile.age, selectedProfile.description);
            v.toggleStageFadeAnim();
        }, 400);
        */
    }

    registerListeners() {
        this.view.registerSubmitBtnListener(this.handleSubmitBtnClick);
    }
}