import Header, {MenuItems} from "../../../Header.js";
import BaseView from "../../BaseView.js";
import {requiredParameter} from "../../../Utils/Tools.js";

const TAG = "ProfilForslagView";

export default
class ProfilForslagView extends BaseView {

    constructor() {
        super("stage-profil-forslag");
    }

    init() {
        Header.render(MenuItems.MATCH);
        this.setInnerHtml(ProfilForslagView.#renderInputForm());
    }

    static #renderProfile(name, imgUrl, age, description) {
        return `
            <section id="profile" class="profile">
                <section class="profile__name">
                    <h3>${name}</h3>
                </section>
                <section class="profile__profileImg">
                    <img src="${imgUrl}" alt="Profile picture" />
                </section>
                <section class="profile__age">
                    <h6>Age:</h6>
                    ${age}
                </section>
                <section class="profile__description">
                    <h6>Description:</h6>
                    ${description}
                </section>
            </section>
        `
    }

    static #renderInputForm() {
        return `
             <section class="input_form">
                <form>
                    <input id="input_form__age" class="input_form__age" type="text" placeholder="Alder" />
                    <label for="input_form__gender">Hvilket kjønn er du interessert i?</label>
                    <select id="input_form__gender">
                        <option value="FEMALE">Kvinner</option>
                        <option value="MALE">Menn</option>
                    </select>
                    <button type="submit" id="input_form__submit">Start</button>
                </form>
            </section>
        `;
    }

    registerSubmitBtnListener(listener = requiredParameter(TAG, "listener", "registerSubmitBtnListener()")) {
        document.getElementById("input_form__submit").onclick = () => listener();
    }

    getAgeInput() {
        return document.getElementById("input_form__age").value;
    }

    getGenderInput() {
        return document.getElementById("input_form__gender").value;
    }

    displayProfile(name, imgUrl, age, description) {
        this.stage.innerHTML = ProfilForslagView.#renderProfile(name, imgUrl, age, description);
    }
}