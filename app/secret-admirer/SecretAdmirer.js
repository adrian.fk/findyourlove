import secretAdmirerController from "./SecretAdmirerController.js";

/**
 *      Acts as a launch command for this use-case
 *
 * @type {secretAdmirerController}
 */

const controller = new secretAdmirerController();
controller.init();