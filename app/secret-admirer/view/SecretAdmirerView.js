import {requiredParameter} from "../../../Utils/Tools.js";
import Header, {MenuItems} from "../../../Header.js";

const TAG = "SecretAdmirerView";

export default
class SecretAdmirerView {

    constructor() {
        this.stage = document.getElementById("stage-sw");
    }

    setStage(stage = requiredParameter(TAG, "stage", "setStage()")) {
        this.stage = stage;
    }

    init() {
        Header.render(MenuItems.SECRET_WONDERER);
    }

    clearView() {
        this.stage.innerHTML = "";
    }

    displayProfile(name, imgUrl, age, description) {
        this.stage.innerHTML = SecretAdmirerView.#renderProfile(name, imgUrl, age, description);
    }

    registerCardClickListener(cardID = requiredParameter(TAG, "cardID", "registerCardClickListener()"),
                              listener = requiredParameter(TAG, "listener", "registerCardClickListener")) {
        const card = document.getElementById(`card-${cardID}`);
        if (card) card.onclick = () => listener(cardID);
        else throw new Error(`Card ID: ${cardID} could not be found.`);
    }

    displayCard(id = requiredParameter(TAG, "id", "displayCard()")) {
        // This is much more performant than +=
        this.stage.insertAdjacentHTML("beforeend", SecretAdmirerView.#renderCard(id));
    }

    triggerCardClickedAnim(cardID) {
        const card = document.getElementById("card-" + cardID);
        const stage = document.getElementById("stage-sw");
        stage.classList.toggle("fade");
        card.classList.toggle("clicked");
        card.classList.remove("card-hover");
    }

    static #renderCard(id) {
        return `
            <section id="card-${id}" class='card card-hover'>
                <h1>?</h1>
            </section>
        `
    }

    static #renderProfile(name, imgUrl, age, description) {
        return `
            <section id="profile" class="profile">
                <section class="profile__name">
                    <h3>${name}</h3>
                </section>
                <section class="profile__profileImg">
                    <img src="${imgUrl}" alt="Profile picture" />
                </section>
                <section class="profile__age">
                    <h6>Age:</h6>
                    ${age}
                </section>
                <section class="profile__description">
                    <h6>Description:</h6>
                    ${description}
                </section>
            </section>
        `
    }
}