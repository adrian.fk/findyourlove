import Profiles from "../../interactor/Profiles.js";
import SecretAdmirerView from "./view/SecretAdmirerView.js";

const NUM_CARDS = 4;

let cardClicked = false;

export default
class secretAdmirerController {

    constructor() {
        this.view = new SecretAdmirerView();
    }

    handleCardClick(cardID) {
        if (cardClicked === true) return;
        cardClicked = true;
        const v = new SecretAdmirerView();

        v.triggerCardClickedAnim(cardID);

        let rdmProfileIndex = Math.floor(Math.random() * 6);
        let profiles = new Profiles().getProfiles();
        let selectedProfile = profiles[rdmProfileIndex];


        //set timeout 400ms - to wait for anim
        setTimeout(
            () => {
                v.displayProfile(selectedProfile.name, selectedProfile.imgUrl, selectedProfile.age, selectedProfile.description);
                v.stage.classList.toggle("fade");
            },
            400
        )
    }

    init() {
        this.view.init();
        this.view.clearView();
        for (let i = 0; i < NUM_CARDS; i++) {
            this.view.displayCard(i);
            this.view.registerCardClickListener(i, this.handleCardClick);
        }
    }
}