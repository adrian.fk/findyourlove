export default
class BaseView {

    constructor(stageID) {
        if (stageID) this.stage = document.getElementById(stageID);
    }

    setStage(stageID) {
        this.stage = document.getElementById(stageID);
    }

    getStage() {
        return this.stage;
    }

    setInnerHtml(html) {
        this.stage.innerHTML = html;
    }

    toggleStageFadeAnim() {
        if (this.stage) this.stage.classList.toggle("fade");
    }

    appendInnerHtml(html) {
        this.stage.insertAdjacentHTML("beforeend", html);
    }
}