import Profiles from "../../interactor/Profiles.js";
import alleProfilerView from "./view/AlleProfilerView.js";

export default
class alleProfilerController {

    constructor() {
        this.profiles = new Profiles();
        this.view = new alleProfilerView();
    }

    init() {
        this.view.init();
        this.view.clearView();
        /*
        this.profiles.getProfiles().forEach(
            (profile, index) => {
                this.view.displayProfile(profile.name, profile.imgUrl, profile.age, profile.description);
            }
        );
         */
        const profiles = this.profiles.getProfiles();
        for (let i = 0; i < profiles.length; i++) {
            this.view.displayProfile(profiles[i].name, profiles[i].imgUrl, profiles[i].age, profiles[i].description);
        }
    }
}