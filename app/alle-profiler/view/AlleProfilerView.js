import {requiredParameter} from "../../../Utils/Tools.js";
import Header, {MenuItems} from "../../../Header.js";

const TAG = "alleProfilerView";

export default
class alleProfilerView {

    constructor() {
        this.stage = document.getElementById("stage-alle-profiler");
    }

    setStage(stage = requiredParameter(TAG, "stage", "setStage()")) {
        this.stage = stage;
    }

    init() {
        Header.render(MenuItems.ALL_PROFILES);
    }

    clearView() {
        this.stage.innerHTML = "";
    }

    displayProfile(name, imgUrl, age, description) {
        this.stage.innerHTML += alleProfilerView.#renderProfile(name, imgUrl, age, description);
    }

    static #renderProfile(name, imgUrl, age, description) {
        return "<section id=\"profile\" class=\"profile\">\n" +
            "            <section class=\"profile__name\">\n" +
            `                <h3>${name}</h3>\n` +
            "            </section>\n" +
            "            <section class=\"profile__profileImg\">\n" +
            `                <img src=\"${imgUrl}" alt=\"Profile picture" />\n` +
            "            </section>\n" +
            "            <section class=\"profile__age\">\n" +
            "                <h6>Age:</h6>\n" +
            `                ${age}` +
            "            </section>\n" +
            "            <section class=\"profile__description\">\n" +
            "                <h6>Description:</h6>\n" +
            `                ${description}` +
            "            </section>\n" +
            "        </section>"
    }
}