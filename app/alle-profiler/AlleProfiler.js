import alleProfilerController from "./AlleProfilerController.js";

/**
 *      Acts as a launch command for this use-case
 *
 * @type {alleProfilerController}
 */

const controller = new alleProfilerController();
controller.init();