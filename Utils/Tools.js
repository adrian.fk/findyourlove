export function requiredParameter(TAG, parameterName, name) {
    throw new Error(`${TAG} :: ${name} missing parameter ${parameterName}`);
}